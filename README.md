# SERGHEI

Simulation Environment for Geomorphology, Hydrodynamics and Ecohydrology in Integrated form

[![doi](https://zenodo.org/badge/DOI/10.5281/zenodo.8159947.svg)](https://doi.org/10.5281/zenodo.8159947)
[![BSD3 License](https://img.shields.io/badge/License-BSD_3--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)
[![doi](https://img.shields.io/badge/rsd-serghei-00a3e3.svg)](https://helmholtz.software/software/serghei)
[![fair-software.eu](https://img.shields.io/badge/fair--software.eu-%E2%97%8F%20%20%E2%97%8F%20%20%E2%97%8F%20%20%E2%97%8F%20%20%E2%97%8B-yellow)](https://fair-software.eu)


# Dependencies

SERGHEI has the following dependencies:

+ [Kokkos](https://github.com/kokkos/kokkos) handles the
parallelization
+ [Parallel NetCDF](https://github.com/Parallel-NetCDF/PnetCDF) writes
output files
+ We use [R](https://www.r-project.org/) scripts for postprocessing (optional)

Both Kokkos and PNetCDF are linked as git submodules in this project. If you are unfamiliar with submodules, you can simply clone this repo together with the submodules with the `git clone --recurse-submodules` command.

PnetCDF is often available in Linux distributions via package managers, and also as software modules in HPC systems. It is recommended to use these system wide installations, and only fall back on building the PNetCDF from source if there's no other option.

# Building SERGHEI with CMake
The CMake workflow is the recommended approach to build SERGHEI. This assumes you have cloned the Kokkos submodule.

1. Build Kokkos
```
bash buildKokkos [GPU_ARCHITECTURE]
``` 
The `GPU_ARCHITECTURE` argument is optional (it defaults to a shared memory CPU architecture). If provided it must be a valid [GPU architecture string as defined by Kokkos](https://kokkos.org/kokkos-core-wiki/keywords.html?highlight=ampere80#architectures), and matching your GPU architecture. 

This configures and builds Kokkos with the target backend and architecture.

2. Configure and build SERGHEI
```
cmake . [-DSERGHEI_OPTION -DSERGHEI_OPTION ...]
make
```

Note that when builing SERGHEI, there is no backend/architecture selection (this was done in the Kokkos build step).

Build options for SERGHEI are passed to CMake as usual, e.g., `-DSERGHEI_WRITE_HZ=ON`. Read the [documentation on the available build options](https://gitlab.com/serghei-model/serghei/-/wikis/CMake-build-options).

# Non-Cmake installation

This procedure is deprecated, and only partially maintained for legacy reasons. It will be progressively phased-out.

## Environment configuration
The environment needs to be configured properly for this procedure to be consistent.

### Automatic configuration
SERGHEI provides the `scripts/configure.sh` script to help you configure your environment for SERGHEI, including setting up environment variables. You can simply run
```bash
bash ./scripts/configure.sh -a
```
which will fetch the dependencies and install them in your local SERGEHI directory.
In case you have already the dependencies and wish to use existing paths, take a look at
```
bash ./scripts/configure.sh --help
```

### Understanding the (manual) environment configuration 
You need to set the SERGHEIPATH environment variable to your local SERGHEI root path. 
```
export SERGHEIPATH=/path/to/serghei
```
Keep in mind doing so is not a persisting configuration.
For persisting configuration, you can set this in your `.bashrc` file. 

Paths in the build scripts, but also in other workflows in SERGHEI use the `SERGHEIPATH` environment variable.

## Building SERGHEI with Make
SERGHEI can be built through `make`. Before attempting to compile, the correct paths pointing to `Kokkos` and `PNetCDF` must be included in the `src/Makefile`. The default paths for these are `$SERGHEIPATH/Kokkos` and `$SERGHEIPATH/PnetCDF` or system paths. If you have these dependencies elsewhere, point the variable `PNETCDF_PATH` to the `PNetCDF` base folder and the variable `KOKKOS_PATH` to the `Kokkos` base folder. The automatic environment configuration should also do this for you.

To build SERGHEI, use either one of these:

+ `make arch=cpu`: compiles the code for CPU (OpenMP + MPI)
+ `make arch=gpu device=DEVICESM`: compiles the code for GPU (CudaUVM + MPI). DEVICESM is the keyword for the device architecture for your GPUs. The list of architecture keywords can be found 
[here](https://github.com/kokkos/kokkos/wiki/Compiling#table-43-architecture-variables). Commonly used may be, for example Ampere80, Volta70, Pascal61, etc.

This places an executable `serghei` into the directory `$SERGHEITPATH/bin` directory.

Further commands:
+ `make clean`: cleans the program objects and the exe file
+ `make kclean`: cleans the prgram objects, the exe file and the Kokkos
objects

### Model component compilation flags
SERHGEI's Makefile includes flags to control which model components are compiled. The default configuration of SERGHEI components is hardcoded. These flags allow you to control which model components are actually compiled. The possible flags are commented inside the Makefile, therefore, unless you either uncomment some of them, or pass them to Make through the command line, you will get the default compilation configuration.
You can control through the command line the setup, for example 
```
$ make arch=cpu SERGHEI_SUBSURFACE_MODEL=1
``` 
will compile SERGHEI for CPUs, including the subsurface solver. 

Another example, to compile for Volta GPUs, including subsurface and particle tracking modules is
```
$ make arch=gpu device=Volta70 SERGHEI_SUBSURFACE_MODEL=1 SERGHEI_PARTICLE_TRACKING=1
``` 
Take a look at the [full list of compilation flags](https://gitlab.com/serghei-model/serghei/-/wikis/User-Guide/Build-flags).


# Running SERGHEI

Once installed, SERGHEI can be invoked by:

```
$ mpirun -n N ./serghei inputDir/ outputDir/ M
```

where

+ `N`: number of MPI tasks (subdomains). This must be in accordance
with the partition chosen in `parameters.input`
+ `inputDir`: directory where the input files are located
+ `outputDir`: directory where the output files will be located
+ `M`: number of threads (OpenMP) or number of GPUs per resource set
(GPUs)

## Examples and test cases

Take a look at the [collection of test cases](https://gitlab.com/serghei-model/serghei-tests).

To run the test case located at `cases/paraboloid2`, execute SERGHEI with 

```
$ mpirun -n 2 /path/to/serghei/bin/serghei ../cases/paraboloid2/ output/ 4
```

Depending on the architecture, this command causes different things to
happen:

1. If the code has been compiled for CPU, this means that it would be
2 subdomains (MPI tasks) parallelized with 4 threads per subdomain
(OpenMP).
2. If the code has been compiled for GPU, this means that it would be
8 subdomains (MPI tasks). The code is run on 2 nodes, each of them
containing 4 GPUs.

Similarly, the use of `mpirun` is conditioned to the execution with
MPI and the corresponding architecture. For example, the code can be
run just using:

```
/path/to/serghei/bin/serghei ./cases/paraboloid2/ output/ 1
```

# Known issues

+ The `clang` compiler may fail to correctly load the OpenMP
library. Thus, if SERGHEI is compiled with `clang`, OpenMP may not be
available.
+ `gcc-10` has trouble compiling Parallel NetCDF and throws a type
  mismatch errors. The errors can be turned into warnings by passing
  ```
  FCFLAGS="-fallow-argument-mismatch" FFLAGS="-fallow-argument-mismatch"
  ```
  to `configure` and `make`. See [this github issue](https://github.com/Unidata/netcdf-fortran/issues/212).

# How to cite 
Please cite the software using the corresponding [SERGHEI Zenodo DOI](https://doi.org/10.5281/zenodo.8159947), and if necessary with the specific release DOI.

You can refer to the [SERGHEI-SWE paper](https://gmd.copernicus.org/articles/16/977/2023/) for the shallow water module SERGHEI-SWE.
```
@Article{Caviedes2023,
AUTHOR = {Caviedes-Voulli\`eme, D. and Morales-Hern\'andez, M. and Norman, M. R. and \"Ozgen-Xian, I.},
TITLE = {SERGHEI (SERGHEI-SWE) v1.0: a performance portable high-performance parallel-computing shallow-water solver for hydrology and environmental hydraulics},
JOURNAL = {Geoscientific Model Development Discussions},
VOLUME = {16}
YEAR = {2023},
PAGES = {977--1008},
URL = {https://gmd.copernicus.org/articles/16/977/2023/},
DOI = {10.5194/gmd-16-977-2023}
}
```
