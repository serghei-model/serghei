# Contributing to SERGHEI

First, if you are reading this, we are very happy that you are interested in contributing. Thanks for the interest.

## Issues and questions
Before raising questions, have a look at the [SERGHEI Wiki](https://gitlab.com/serghei-model/serghei/-/wikis/home) where the FAQs find their way into the main documentations.

Feel free to use the [SERGHEI issue tracker](https://gitlab.com/serghei-model/serghei/-/issues). You can browse open and closed issues which may address your questions/issues. Don't be shy to open a new issue, whether it is to report a bug, request a feature, or simply raise questions.

### Reporting bugs
If you think you may have encountered a bug, please raise the issue identifying the bug and include as many details as possible, including:
- descriptive title
- include a clear pointer to the SERGHEI code version and/or commit hash in which the bug manifests (you can use the  `git describe` for this)
- provide input files (if possible), especially  if the bug only manifests on a specific case. If input files are too large, you can provide a link.
- provide the error report (hopefully SERGHEI will provide one), ideally as Markdown in your bug report text body.
- if you face compilations issues, please provide information on your Linux distribution, compiler and dependency versions.
- let us know if the problem started happening with a new version, a new case, a new system, etc.

### Feature requests and suggestions
We are happy to lear which features would be good to have in SERGHEI. Feel free to open an issue for this, following these guidelines:
- check whether the feature has been already requested
- provide a clear description of the feature you need, and why it would be relevant and useful for SERGHEI users


## Contributing to the code
We are happy to have community contributions. If you wish to contriubte to the code, keep the following in mind:

### Resources
Check the [Developer Guide in the SERGHEI Wiki](https://gitlab.com/serghei-model/serghei/-/wikis/Developer-Guide/Programming-Guide). This explains some of the most relevant and potentially convoluted aspects of the code. 

### License
SERGHEI has a quite permissive [open source license](https://gitlab.com/serghei-model/serghei/-/blob/master/LICENSE?ref_type=heads). Please make sure you agree with this license before contributing. The project is intended to be fully open source to guarantee community access. If your contribution for some reason cannot be fully open source (or perhaps has a temporary embargo), reach out to the maintainers before contributing.

Of course, contributions will be recognised both by the native contributor logs in GitLab, and in the [CONTRIBUTORS.md]() file.

### Forking, branching and merge requests
- Only the core maintainer team has write access to the SERGHEI repository. In order to contribute, please fork out and create a merge request. You can do this even early during development, which facilitates interacting with the core maintainer team early on. 

### Testing and CI/CD
- SERGHEI enforces CI/CD pipelines, which you can find openly in the GitLab repository. Merge requests which fail the pipeline will be automatically rejected, to ensure the sanity of the code base.

- Merge requests with new features should provide CI/CD steps at least to build SERGHEI with those features, and to perform a basic sanity test on the features.

- We also maintain continuous benchmarking on HPC systems. It is expected that relevant features will be incorporated into that benchmarking.
