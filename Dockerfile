# syntax=docker/dockerfile:1

FROM debian:latest
RUN apt-get update;\
apt-get -y install bc cmake pkg-config cmake-data libpnetcdf-dev openmpi-bin libopenmpi-dev git g++ netcdf-bin nco
WORKDIR /app
#COPY . .
#RUN yarn install --production
#CMD ["node", "src/index.js"]
#EXPOSE 3000
