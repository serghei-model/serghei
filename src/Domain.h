
#ifndef _DOMAIN_H_
#define _DOMAIN_H_

#include "define.h"
#include "Parallel.h"
#include "geometry.h"
#include "Indexing.h"


class Domain {

public:

  real cfl;
  double simLength=0;
  double startTime=0;
  double endTime=SERGHEI_NAN;
  double etime=0;

  real dt=0;

  SergheiTimers mutable timers;

  //raster variables
  int nx_glob;
  int ny_glob;
  int nx;         // physical number of cells in x-direction
  int ny;         // physical number of cells in y-direction
  int nCellMem = 0;   // physical cells + halo cells
  int nCell = 0;    // physical cells
  int nCellValid = 0; // cells which have data
  double xll = 0; // southwest corner x-coordinate
  double yll = 0; // southwest corner y-coordinate
  #if SERGHEI_MESH_UNIFORM
  real dxConst;  // resolution
  #endif

  //flags to see if the subdomain touch with either a East, West, South or North boundaries
  int iE=0;
  int iW=0;
  int iS=0;
  int iN=0;

  // global (reduced) variables
  real areaGlobal;
  int nCellValidGlobal;
  int nCellGlobal;

  // other variables
  int BCtype;
  int isRain = 0;
  int isEvap = 0;

  int nIter;
  int countIterDt;
  int cg_iter;

  real area;
  int id;  // subdomain ID


  realArr globalBuffer;

  geometry::point extent[2];

  // this is purposely programmed to fail at compilation time if !SERGHEI_MESH_UNIFORM because the alternative is not implemented
  #if SERGHEI_MESH_UNIFORM
  KOKKOS_INLINE_FUNCTION real dx() const{
    return(dxConst);
  }
  #endif


  KOKKOS_INLINE_FUNCTION void domainArea(){
       area = cellArea()*nCellValid;
   };

  KOKKOS_INLINE_FUNCTION void unpackIndices(int const iGlob, int &j, int &i) const{
    unpackIndicesUniformGrid(iGlob,ny,nx,j,i);
  };

  // this is purposely programmed to fail at compilation time if !SERGHEI_MESH_UNIFORM because the alternative is not implemented
  #if SERGHEI_MESH_UNIFORM
  KOKKOS_INLINE_FUNCTION geometry::point getCellCenter(int i, int j) const{
    geometry::point p;
    p(_X) = extent[0](_X) + i*dxConst ;
    p(_Y) = extent[1](_Y) - j*dxConst ;
    return(p);
  #endif
  }

  KOKKOS_INLINE_FUNCTION geometry::point getCellCenter(int iGlob) const{
    int i,j;
    unpackIndices(iGlob,j,i);
    return(getCellCenter(i,j));
  }

  // this is purposely programmed to fail at compilation time if !SERGHEI_MESH_UNIFORM because the alternative is not implemented
  #if SERGHEI_MESH_UNIFORM
  KOKKOS_INLINE_FUNCTION real cellArea() const{
    return(dx()*dx());  // WARNING UCM
  }
  #endif


// this is purposely programmed to fail at compilation time if !SERGHEI_MESH_UNIFORM because the alternative is not implemented
#if SERGHEI_MESH_UNIFORM
  KOKKOS_INLINE_FUNCTION void getMatrixIndicesForPoint(const geometry::point &p, int &i, int &j) const{
    // get i,j coordinates of the cell containing the point
    // this only works on a raster-order grid, where j is zero at NORTH boundary
    i = floor((p(_X)-extent[0](_X))/dxConst);
    j = floor((extent[1](_Y)-p(_Y))/dxConst);
  };

  KOKKOS_INLINE_FUNCTION int getCellForPoint(const geometry::point &p) const{
    int i,j,iGlob;
    getMatrixIndicesForPoint(p,i,j);
    if(i < 0 || i >= nx || j < 0 || j >= ny){
      iGlob = -1;
    }else{
      iGlob = packIndicesUniformGrid(ny,nx,j,i);
    }
    // note: domain decomposition is handled by the i,j coords
    return(iGlob);
  };
  #endif

  KOKKOS_INLINE_FUNCTION int getHaloExtension(const int i, const int j) const {
    return( (hc+j)*(nx+2*hc)+hc+i ); //index for the extended domain (including halo cells)
  };

  KOKKOS_INLINE_FUNCTION int getIndex(int iGlob) const{
    int i,j;
    unpackIndices(iGlob,j,i);
    return ( getHaloExtension(i,j) );
  };

  KOKKOS_INLINE_FUNCTION int getSubdomainExtension(const Parallel &par, const int i, const int j) const{
    return( (par.j_beg+j)*nx_glob+par.i_beg+i ); //index for the subdomain (par.j_beg+j,par.i_beg+i)
  };

  KOKKOS_INLINE_FUNCTION int getIndexForPoint(const geometry::point &p) const{
    int i,j,ii;
    getMatrixIndicesForPoint(p,i,j);
    if(i < 0 || i >= nx || j < 0 || j >= ny){
      ii = -1;
    }else{
      ii  = getHaloExtension(i,j);
    }
    return(ii);
  };

void initialise() {
   	// Initialize the time
   	nIter = 0;
		countIterDt=0;
    etime = startTime;
    if(std::isnan(endTime)){
      endTime = startTime + simLength;
    }
    else{
      simLength = endTime - startTime;
    }

    #if SERGHEI_MESH_UNIFORM
      nCell = nx*ny;                    // physical cells in this subdomain
      nCellMem = (ny+2*hc)*(nx+2*hc);   // size of arrays (including halos)
      nCellGlobal = nx_glob * ny_glob;  // physical number of cells across all subdomains
    #endif

    globalBuffer = realArr("globalBuffer", nCellGlobal);
    if(id == 0) std::cout << GOK << "Domain initialised" << std::endl;
  };

void getStatistics(){
    domainArea();
    MPI_Allreduce(&area, &areaGlobal, 1, MPI_DOUBLE , MPI_SUM, MPI_COMM_WORLD);
    MPI_Allreduce(&nCellValid, &nCellValidGlobal, 1, MPI_INT , MPI_SUM, MPI_COMM_WORLD);
};

  int buildDomainDecomposition(Parallel &par) {

    int ierr=1;

    if (par.nranks != par.nproc_x*par.nproc_y) {
      std::cerr << RERROR "ERROR: nproc_x*nproc_y != nranks" << std::endl;
      std::cerr << RERROR << par.nproc_x << " " << par.nproc_y << " " << par.nranks << std::endl;
      exit(-1);
    }

    //Get my x and y process grid ID
    par.px = par.myrank % par.nproc_x;
    par.py = par.myrank / par.nproc_x;

    // name subdomain
    id = par.myrank;

    //Get my beginning and ending global indices
    double nper;
    nper = ((double) nx_glob)/par.nproc_x;
    par.i_beg = (long) round( nper* par.px    );
    par.i_end = (long) round( nper*(par.px+1) )-1;
    nper = ((double) ny_glob)/par.nproc_y;
    par.j_beg = (long) round( nper* par.py    );
    par.j_end = (long) round( nper*(par.py+1) )-1;

    // Determine my number of grid cells
    nx = par.i_end - par.i_beg + 1;
    ny = par.j_end - par.j_beg + 1;

    // Determine my extent, point 0 is SW, point 1 is NE (standard cartesian)
    extent[0](_X) = xll + par.i_beg*dxConst;
    extent[0](_Y) = yll + ny_glob*dxConst - (par.j_end+1)*dxConst;
    extent[1](_X) = xll + (par.i_end+1)*dxConst;
    extent[1](_Y) = yll + ny_glob*dxConst - (par.j_beg)*dxConst;

    for (int j = 0; j < 3; j++) {
      for (int i = 0; i < 3; i++) {
        int pxloc = par.px+i-1;
        if (pxloc < 0            ) pxloc = pxloc + par.nproc_x;
        if (pxloc > par.nproc_x-1) pxloc = pxloc - par.nproc_x;
        int pyloc = par.py+j-1;
        if (pyloc < 0            ) pyloc = pyloc + par.nproc_y;
        if (pyloc > par.nproc_y-1) pyloc = pyloc - par.nproc_y;
        par.neigh(j,i) = pyloc * par.nproc_x + pxloc;
      }
    }

  	//set topological boundaries
  	if(par.myrank % par.nproc_x ==0) iW=1; //west boundary of the full domain
	  if(par.myrank % par.nproc_x ==par.nproc_x-1) iE=1; //east boundary of the full domain
	  if(par.myrank / par.nproc_x ==0) iN=1; //north boundary of the full domain
	if(par.myrank / par.nproc_x ==par.nproc_y-1) iS=1; //south boundary of the full domain

    // Debug output for the parallel decomposition
    #if SERGHEI_DEBUG_PARALLEL_DECOMPOSITION
      for (int rr=0; rr < par.nranks; rr++) {
        if (rr == par.myrank) {
          std::cerr << GGD "Hello! My Rank is: " << par.myrank << "\n";
          std::cerr << GGD "My domain id is: " << id << std::endl;
          std::cerr << GGD "My proc grid ID is: " << par.px << " , " << par.py << "\n";
          std::cerr << GGD "I have: " << nx << " x " << ny << " grid cells" << "\n";
          std::cerr << GGD "I start at index: " << par.i_beg << " x " << par.j_beg << "\n";
          std::cerr << GGD << "My extent points are " ;
          std::cerr << "(" << extent[0](_X) << "," << extent[0](_Y) << ") ";
          std::cerr << "(" << extent[1](_X) << "," << extent[1](_Y) << ") " << std::endl;
          std::cerr << GGD "My neighbor matrix is:\n";
          for (int j = 2; j >= 0; j--) {
            for (int i = 0; i < 3; i++) {
              std::cerr << std::setw(6) << par.neigh(j,i) << " ";
            }
            std::cerr << "\n";
          }
          std::cerr << "\n";
          }
          ierr = MPI_Barrier(MPI_COMM_WORLD);
      }
      ierr = MPI_Barrier(MPI_COMM_WORLD);
    #endif
    if(par.masterproc) std::cout << GOK << "Domain decomposition" << std::endl;
	  return ierr;
	};



void fetchFieldFromGlobalBuffer(const Parallel &par, realArr &data) const {
  Kokkos::parallel_for("fetch_from_global_buffer", nCell , KOKKOS_CLASS_LAMBDA (int iGlob) {
 	  int i,j;
		unpackIndices(iGlob,j,i);
		int ii1 = getHaloExtension(i,j);
		int ii2 = getSubdomainExtension(par,i,j);
		data(ii1) = globalBuffer(ii2);
	});
}

};


#endif
