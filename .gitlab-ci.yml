image: $CI_REGISTRY_IMAGE/build-image:latest
#image: debian:latest

before_script:
  - export KOKKOS_COMMIT_HASH=$(git submodule status kokkos | awk '{ print $1 }')
  
stages:
  - image
  - kokkos
  - build
  - sanity tests
  - integration tests
  - end-to-end tests

default:
  tags:
    - '$CI_DEFAULT_TAG_SERGHEI'   # should be defined in the GitLab instance as a CI/CD variable
#    - saas-linux-medium-amd64

variables:
  FORCE_DOCKER_IMAGE:
    value: "false"
    options:
      - "false"
      - "true"
    description: "Force the creation of a new Docker image for this branch."
  
  GIT_SUBMODULE_STRATEGY: recursive
#  GIT_SUBMODULE_DEPTH: 1

build-kokkos:
  stage: kokkos
  rules:
    - when: always
  artifacts:
    #untracked: true
    paths:
      - kokkos
    when: on_success
    expire_in: 1 month
  cache:
    key: ${CI_COMMIT_REF_SLUG}-${KOKKOS_COMMIT_HASH}
    paths:
      - kokkos
  script:
    - bash buildKokkos

build-kokkos-kernels:
  stage: kokkos
  dependencies:
    - build-kokkos
  needs: ["build-kokkos"]
  script:
    - bash buildKokkosKernels
  artifacts:
    paths:
      - kokkos-kernels
    when: on_success
    expire_in: 1 month
  cache:
    key: ${CI_COMMIT_REF_SLUG}-kokkos-kernels
    paths:
      - kokkos-kernels
  script:
    - bash buildKokkosKernels

build-default:
  stage: build
  dependencies:
    - build-kokkos
  needs: ["build-kokkos"]
  rules:
    - when: on_success
  artifacts:
    untracked: true
    when: on_success
    expire_in: 1 day
  script:
    - cmake .
    - make -j

build-chezy:
  stage: build
  dependencies:
    - build-kokkos
  needs: ["build-kokkos"]
  artifacts:
    untracked: true
    when: on_success
    expire_in: 1 days
  script:
    - cmake . -DSERGHEI_FRICTION_CHEZY=ON
    - make -j

build-nc-opts:
  stage: build
  dependencies:
    - build-kokkos
  needs: ["build-kokkos"] 
  rules:
  - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    when: on_success
  - when: manual
  script:
    - cmake . -DSERGHEI_NC_64BIT_DATA=ON -DSERGHEI_NC_FLOAT=ON -DSERGHEI_NC_ENABLE_NAN=OFF
    - make -j

build-nc-input:
  stage: build
  dependencies:
    - build-kokkos
  needs: ["build-kokkos"] 
  rules:
  - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    when: on_success
  - when: manual
  artifacts:
    untracked: true
    when: on_success
    expire_in: 1 days
  script:
    - cmake . -DSERGHEI_INPUT_NETCDF=ON
    - make -j

build-opts:
  stage: build
  dependencies:
    - build-kokkos
  needs: ["build-kokkos"]
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: on_success
    - when: manual
  script:
#    - bash buildKokkos 
    - cmake . -DSERGHEI_WRITE_HZ=ON -DSERGHEI_MAXFLOOD=ON -DSERGHEI_WRITE_SUBDOMS=ON
    - make -j

build-default-make:
    stage: build
    needs: []
    script: 
    - export SERGHEIPATH=$(pwd)
    - cd src
    - make -j -f Makefile.serghei 

build-nc-opts-make:
    stage: build
    needs: []
    rules:
      - if: $CI_PIPELINE_SOURCE == "merge_request_event"
        when: on_success
      - when: manual
    script: 
    - export SERGHEIPATH=$(pwd)
    - cd src
    - make -j -f Makefile.serghei SERGHEI_NC_MODE=NC_64BIT_DATA SERGHEI_NC_REAL=NC_FLOAT SERGHEI_INPUT_NETCDF=1 SERGHEI_NC_ENABLE_NAN=1

build-io-opts-make:
    stage: build
    needs: []
    rules:
      - if: $CI_PIPELINE_SOURCE == "merge_request_event"
        when: on_success
      - when: manual
    script: 
    - export SERGHEIPATH=$(pwd)
    - cd src
    - make -j -f Makefile.serghei SERGHEI_WRITE_HZ=1 SERGHEI_MAXFLOOD=1 SERGHEI_WRITE_SUBDOMS=1

build-lpt:
    stage: build
    rules:
      - when: always
      - if: $CI_PIPELINE_SOURCE == "merge_request_event"
        when: on_success
      - when: manual
    script: 
    - export SERGHEIPATH=$(pwd)
    - cd src
    - make -j -f Makefile.serghei SERGHEI_LPT=1
build-lpt-not-evolution:
    stage: build
    rules:
      - when: always
      - if: $CI_PIPELINE_SOURCE == "merge_request_event"
        when: on_success
      - when: manual
    artifacts:
      untracked: true
      when: on_success
      expire_in: 1 days
    script: 
    - export SERGHEIPATH=$(pwd)
    - cd src
    - make -j -f Makefile.serghei SERGHEI_LPT=1 SERGHEI_HYDRODYNAMIC_NOT_EVOLUTION=1

build-subsurface:
  stage: build
  needs: ["build-kokkos", "build-kokkos-kernels"]
  artifacts:
    untracked: true
    when: on_success
    expire_in: 1 days
  script:
    - cmake . -DSERGHEI_SUBSURFACE_MODEL=ON
    - make -j

##### RUNTIME TESTS ######
damBreakX-serial:
  stage: sanity tests
#  dependencies:
#    - build-default
  needs: ["build-default"]
  rules:
    - when: always
  script:
    - echo "Test damBreakX"
    - ./bin/tests/dambreak damBreakX 1
    - git clone https://gitlab.com/serghei-model/serghei-tests/ci-tests-reference-solutions/dambreakx-ref.git 
    - bash ./scripts/compareSolution.sh damBreakX dambreakx-ref    

# subsurface flow tests
subsurface-closed-column:
  stage: sanity tests
  dependencies:
    - build-subsurface
  needs: ["build-subsurface"]
  script:
    - echo "Testing subsurface closed column"
    - ./bin/tests/closedColumnSubsurface column 1
    - git clone https://gitlab.com/serghei-model/serghei-tests/subsurface/ci-tests-reference-solutions/closedcolumn.git 
    - bash ./scripts/compareSolution.sh -m gw column closedcolumn/

damBreakCircle-omp:
  stage: integration tests
  needs: ["build-default", "damBreakX-serial"]
  script:
    - echo "Testing damBreakCircle"
    - ./bin/tests/dambreak damBreakCircle 4
    - git clone https://gitlab.com/serghei-model/serghei-tests/ci-tests-reference-solutions/dambreakcircle-ref.git 
    - bash ./scripts/compareSolution.sh damBreakCircle dambreakcircle-ref

damBreakCircle-mpi:
  stage: integration tests
  dependencies:
    - build-default
  needs: ["build-default"]
  script:
    - echo "Testing damBreakCircle MPI"
    - export OMPI_ALLOW_RUN_AS_ROOT=1
    - export OMPI_ALLOW_RUN_AS_ROOT_CONFIRM=1
    - mpirun -np 2 --oversubscribe ./bin/tests/dambreak damBreakCircle 1 100 2 1
    - git clone https://gitlab.com/serghei-model/serghei-tests/ci-tests-reference-solutions/dambreakcircle-ref.git 
    - bash ./scripts/compareSolution.sh damBreakCircle dambreakcircle-ref

parabolicBowl-planar-omp:
  stage: integration tests
  dependencies:
    - build-default
  needs: ["build-default", "damBreakX-serial"]
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: on_success
    - when: manual
  script:
    - echo "Testing planar parabolic bowl"
    - ./bin/tests/parabolicBowl planar 4
    - git clone https://gitlab.com/serghei-model/serghei-tests/ci-tests-reference-solutions/parabolicBowl-planar-ref.git 
    - bash ./scripts/compareSolution.sh planar parabolicBowl-planar-ref

parabolicBowl-radial-omp:
  stage: integration tests
  dependencies:
    - build-default
  needs: ["build-default", "damBreakX-serial"]
  script:
    - echo "Testing radial symmetric parabolic bowl"
    - ./bin/tests/parabolicBowl radialSymmetric 4
    - git clone https://gitlab.com/serghei-model/serghei-tests/ci-tests-reference-solutions/parabolicBowl-radial-ref.git 
    - bash ./scripts/compareSolution.sh radialSymmetric parabolicBowl-radial-ref

damBreakTriangularSill:
  stage: integration tests
  dependencies:
    - build-nc-input
  needs: ["build-nc-input"]
  script:
    - echo "Testing dam break over a triangular sill"
    - git clone https://gitlab.com/serghei-model/serghei-tests/experimental/damBreakTriangularSill_small.git
    - cd damBreakTriangularSill_small 
    - ../bin/serghei ./input/ ./output/ 4
# compare the solution against a reference computed from a raster input
    - git clone https://gitlab.com/serghei-model/serghei-tests/ci-tests-reference-solutions/damBreakTriangularSill_small_raster-ref.git
    - bash ../scripts/compareSolution.sh output damBreakTriangularSill_small_raster-ref
# compare the solution against a reference computed from a netcdf input
    - git clone https://gitlab.com/serghei-model/serghei-tests/ci-tests-reference-solutions/damBreakTriangularSill_small_netcdf-ref.git
    - bash ../scripts/compareSolution.sh output damBreakTriangularSill_small_netcdf-ref

circularVortex:
  stage: integration tests
  dependencies:
    - build-lpt-not-evolution
  needs: ["build-lpt-not-evolution"]
  script:
    - echo "Testing circular vortex for LPT module"
    - git clone https://gitlab.com/serghei-model/serghei-tests/lpt/circular-case.git
    - cd circular-case 
    - ../bin/serghei ./input/ ./output/ 4
# compare the solution against a reference computed from a raster input
    - git clone https://gitlab.com/serghei-model/serghei-tests/lpt/ci-test-reference-solutions.git
    - bash ../scripts/compareSolution_lpt.sh output ci-test-reference-solutions/circular_vortex

########## END-TO-END-TESTS ###########
govindaraju-rain:
  stage: end-to-end tests
  dependencies:
    - build-chezy
  needs: ["build-chezy"]
  script:
    - echo "Testing Govindaraju rainfall on a channel"
    - git clone https://gitlab.com/serghei-model/serghei-tests/analytical/govindaraju.git 
    - cd govindaraju 
    - ../bin/serghei ./input.light/ ./output/ 4
    - git clone https://gitlab.com/serghei-model/serghei-tests/ci-tests-reference-solutions/govindaraju-ref.git
    - bash ../scripts/compareSolution.sh output govindaraju-ref


macdonald-super-to-subcritical:
  stage: end-to-end tests
  dependencies:
    - build-default
  needs: ["build-default", "damBreakX-serial"]
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: on_success
    - when: manual
  script:
    - echo "Testing MacDonald supercritical to subcritical steady flow"
    - git clone https://gitlab.com/serghei-model/serghei-tests/analytical/swashes/steady-state/long-channel-supercritical-to-subcritical.git 
    - cd long-channel-supercritical-to-subcritical 
    - ../bin/serghei ./input/ ./output/ 4
    - git clone https://gitlab.com/serghei-model/serghei-tests/ci-tests-reference-solutions/long-channel-supercritical-to-subcritical-ref.git 
    - bash ../scripts/compareSolution.sh output long-channel-supercritical-to-subcritical-ref

#monaiTsunami-omp:
#  stage: end-to-end tests
#  dependencies:
#    - build-default
#  needs: ["build-default", "damBreakX-serial"]
#  script:
#    - echo "Monai Tsunami experiment"
#    - git clone https://gitlab.com/serghei-model/serghei-tests/experimental/tsunamimonai.git 
#    - cd tsunamimonai
#    - ../bin/serghei ./input/ ./output/ 4

monaiTsunami-mpi:
  stage: end-to-end tests
  dependencies:
    - build-default
  needs: ["build-default", "damBreakX-serial"]
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: on_success
    - when: manual
  script:
    - echo "Monai Tsunami experiment"
    - git clone https://gitlab.com/serghei-model/serghei-tests/experimental/tsunamimonai.git 
    - cd tsunamimonai
    - "sed -i '\/parNx\/c\\parNx : 2' input/parameters.input"
    - "sed -i '\/parNy\/c\\parNy : 1' input/parameters.input"
    - export OMPI_ALLOW_RUN_AS_ROOT=1
    - export OMPI_ALLOW_RUN_AS_ROOT_CONFIRM=1
    - mpirun -np 2 --oversubscribe ../bin/serghei ./input/ ./output/ 1

warrick-inf-1d-openmp:
  stage: end-to-end tests
  dependencies:
    - build-subsurface
  needs: ["build-subsurface", "subsurface-closed-column"]
  script:
    - echo "1D infiltration Warrick problem"
    - git clone https://gitlab.com/serghei-model/serghei-tests/subsurface/analytical/infiltration1d.git
    - cd infiltration1d
    - ../bin/serghei ./input/ ./output/ 4
    - git clone https://gitlab.com/serghei-model/serghei-tests/subsurface/ci-tests-reference-solutions/infiltration1d-ref.git 
    - bash ../scripts/compareSolution.sh -m gw output infiltration1d-ref

create-image:
  stage: image
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - mkdir -p /kaniko/.docker
    - |
      echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" \
      > /kaniko/.docker/config.json
    - |
      /kaniko/executor --context $CI_PROJECT_DIR \
      --dockerfile $CI_PROJECT_DIR/Dockerfile \
      --destination $CI_REGISTRY_IMAGE/build-image:$CI_COMMIT_SHORT_SHA \
      --destination $CI_REGISTRY_IMAGE/build-image:latest
  rules:
    # Run when triggered and a varible is set
    - if: $FORCE_DOCKER_IMAGE == "true"
    # Run when committing to a branch and changing Dockerfile
    - if: $CI_COMMIT_BRANCH
      changes:
        - Dockerfile
    # Run when merging and changing Dockerfile
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        - Dockerfile

#deploy-prod:
#  stage: deploy
#  script:
#    - echo "This job deploys something from the $CI_COMMIT_BRANCH branch."
#  environment: production
