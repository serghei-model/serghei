library(SERGHEI)

casepath=~/serghei_testcases/Hydrology/Catchments/Arnas

outpath=file.path(casepath,"output")

ts = SERGHEI::readSergheiDomainTS(outpath)
plot(ts$Time,ts$RainFlux,t="l")
plot(ts$Time,ts$BoundaryFlow,t="l",xlab="Time [s]",ylab="Q [m3/s]")
