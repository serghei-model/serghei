library(SERGHEI)

basedir="./"

# set directories
outdir = file.path(basedir,"output")
indir = file.path(basedir,"input")

# open time series output data
df=SERGHEI::readSergheiDomainTS(outdir)

myplot=SERGHEI::plotBoundaryOutflow(df,inflow=T)

fname=file.path(basedir,"boundaryFlows.pdf")
ggsave(fname,myplot,width=8,height=6)

# open lines
lines=SERGHEI::readObservationLines(indir,outdir)
line = lines[[2]]
plotline=line$t_100
#SERGHEI::plotLine(plotline,xvar="y",yvar="hu")
plot(plotline$y,plotline$hu,t="b")

# open unit test lines
refdir = file.path(basedir,"unitTest")
refLines = SERGHEI::readObservationLines(indir,refdir)
refLine = refLines[[2]]
plotline=refLine$t_100
lines(plotline$y,plotline$hu,t="b",col="red")

times=c(70,150,600)

getError = function(tt){
  ii=which(names(refLine)==paste0("t_",tt))
  return( sum(refLine[[ii]] - line[[ii]]) )
}

errors = sapply(times,getError)

cat("Inflow BC errors at times ",times,"\n")
cat(errors,"\n")
