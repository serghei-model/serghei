library(raster)

# define this
casedir = "~/serghei/unitTests/ShallowWater/Experimental/urbanDamBreakHighRes/input/"
res = 0.0025


# these are properties of the domain - nothing to choose here
Lx = 36
Ly = 3.60


xBlock = 6.75
dxBlock = 0.8

xTranslation = xBlock+dxBlock/2

xGate = 7.15
dyGate = 1
dyBlock = 1.30

dxHouse = 0.3
dxStreet = 0.1

xCity = xGate + 5
yCity = Ly/2 - 2.5*dxHouse - 2*dxStreet 

slope = 0
dzBlock = 1
dzHouse = 0.3

sideZ = 0.155
sideY = 0.34
sideS = sideZ/sideY

hTank = 0.4
hFlume = 0.011

# generate raster
dem = raster(xmn = 0, xmx = Lx, ymn = 0, ymx = Ly, resolution = c(res,res))

# base level
dem[] = 0

# get coordinates
p = as.data.frame(coordinates(dem))

# side slopes
# y=0
region = p
region = subset(region, region$y <= sideY)
ids = as.numeric(rownames(region)) 
q = as.data.frame(coordinates(region))
side = sideZ -q$y * sideS 
dem[ids] = side

#y=Ly
region = p
region = subset(region, region$y >= Ly-sideY)
ids = as.numeric(rownames(region)) 
q = as.data.frame(coordinates(region))
side = sideZ -(Ly-q$y) * sideS 
dem[ids] = side


# gate blocks
region = p

region = subset(region, region$x >= xBlock & region$x <= xBlock + dxBlock)
region = subset(region, region$y <= dyBlock | region$y >= Ly - dyBlock)

ids = as.numeric(rownames(region)) 

dem[ids] = dzBlock

# city buildings
x0 = xCity + seq(0,4,1)*(dxHouse+dxStreet)
y0 = yCity + seq(0,4,1)*(dxHouse+dxStreet)
for(xx in x0) {
  for(yy in y0){ 
    region = p
    region = subset(region, region$x >= xx & region$x <= xx + dxHouse)
    region = subset(region, region$y >= yy & region$y <= yy + dxHouse)
    ids = as.numeric(rownames(region)) 
    dem[ids] = dzHouse
  }
}

# shift to cooridnates in the reference data
demShift = shift(dem,-xTranslation,-Ly/2)
plot(demShift)
#plot(dem,xlim=c(5,16))
fname = file.path(casedir,"dem.input")
writeRaster(demShift,fname,format="ascii",overwrite=T)
file.rename(file.path(casedir,"dem.asc"),fname )

### Initial conditions
hzini = dem
hzini[] = hFlume
hini = hzini

region = p
region = subset(region, region$x <= xGate)
ids = as.numeric(rownames(region)) 
hzini[ids] = hTank

hini = max(hzini - dem , 0)
#plot(hini,xlim=c(5,16))

demShift = shift(hini,-xTranslation,-Ly/2)
#plot(demShift)
fname = file.path(casedir,"hini.input")
writeRaster(demShift,fname,format="ascii",overwrite=T)
file.rename(file.path(casedir,"hini.asc"),fname )

