caseName=${@:$OPTIND:1}
refName=${@:$OPTIND+1:1}

#caseName=$1
#refName=$2

echo "Comparing LPT solution"

ncdiff $caseName/lpt.nc $refName/lpt.nc -O diff.nc

ncap2 -O -s 'max_x=abs(max(x))' diff.nc max.nc
ncap2 -A -s 'max_y=abs(max(y))' diff.nc max.nc

x=$(ncks -C -H -v max_x -s "%.10f" max.nc)
y=$(ncks -C -H -v max_y -s "%.10f" max.nc)

# error tolerance is higher for subsurface
zero=0.000001

if (( $(echo "$x > $zero" | bc -l) )); then
  echo "X-coordinate error $x is larger than threshold $zero"
  error=1
fi
if (( $(echo "$y > $zero" | bc -l) )); then
  echo "Y-coordinate error $y is larger than threshold $zero"
  error=1
fi

if [ "$error" = 1 ]; then
  echo "LPT comparison test failed"
  exit 1
fi

echo "Maximum error of all LPT variables is below the threshold."

