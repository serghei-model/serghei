#!/bin/bash


mode="swe"

set -e 

while getopts "m:" flag; do
case "$flag" in
    m) mode=$OPTARG;;
		\? ) echo "Usage: $0 [-m swe,gw] path-to-solution path-to-ref-solution"
			exit 0
esac
done

caseName=${@:$OPTIND:1}
refName=${@:$OPTIND+1:1}

#caseName=$1
#refName=$2


if [ "$mode" = "swe" ]; then
echo "Comparing SWE solution"

ncdiff $caseName/output.nc $refName/output.nc -O diff.nc
ncap2 -O -s 'max_h=abs(max(h))' diff.nc max.nc
ncap2 -A -s 'max_u=abs(max(u))' diff.nc max.nc
ncap2 -A -s 'max_v=abs(max(v))' diff.nc max.nc

h=$(ncks -C -H -v max_h -s "%.10f" max.nc)
u=$(ncks -C -H -v max_u -s "%.10f" max.nc)
v=$(ncks -C -H -v max_v -s "%.10f" max.nc)

zero=0.000000000001

if (( $(echo "$h > $zero" | bc -l) )); then
  echo "Depth error $h is larger than threshold $zero"
  error=1
fi
if (( $(echo "$u > $zero" | bc -l) )); then
  echo "Velocity-x error $u is larger than threshold $zero"
  error=1
fi
if ((  $(echo "$v > $zero" | bc -l) )); then
  echo "Velocity-y error $v is larger than threshold $zero"
  error=1
fi

if [ "$error" = 1 ]; then
  echo "SWE comparison test failed"
  exit 1
fi

echo "Maximum error of all surface variables is below the threshold."

fi

### now the subsurface

if [ "$mode" = "gw" ]; then
echo "Comparing GW solution"

ncdiff $caseName/output_subsurface.nc $refName/output_subsurface.nc -O diff.nc

ncap2 -O -s 'max_hd=abs(max(hd))' diff.nc max.nc
ncap2 -A -s 'max_wc=abs(max(wc))' diff.nc max.nc

hd=$(ncks -C -H -v max_hd -s "%.10f" max.nc)
wc=$(ncks -C -H -v max_wc -s "%.10f" max.nc)

# error tolerance is higher for subsurface
zero=0.000001

if (( $(echo "$hd > $zero" | bc -l) )); then
  echo "Pressure error $hd is larger than threshold $zero"
  error=1
fi
if (( $(echo "$wc > $zero" | bc -l) )); then
  echo "Water content error $wc is larger than threshold $zero"
  error=1
fi

if [ "$error" = 1 ]; then
  echo "GW comparison test failed"
  exit 1
fi

echo "Maximum error of all subsurface variables is below the threshold."

fi
